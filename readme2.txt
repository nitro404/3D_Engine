====================================
         COURSE: COMP 4501
     ASSIGNMENT: Assignment 2
====================================
           NAME: Kevin Scroggins
     STUDENT ID: 100679071
         E-MAIL: nitro404@gmail.com
       DATE DUE: March 6, 2012
 DATE SUBMITTED: March 13, 2012
====================================

INSTRUCTIONS
 + See file "readme.txt" for additional information.

CONTROLS
 + WSAD to move around
 + Space / Q to move up
 + Z / E to move down
 + Left click to throw cubes
 + Escape to exit / go back

NOTES
 + PhysX fully implemented with collisions for:
   - Camera
   - Cubes
   - Static Geometry
   - Terrain (although slightly misaligned, it still works)
