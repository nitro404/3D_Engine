import java.util.Vector;

public class Waterfall extends Pool {
	
	public Waterfall(UniversalObject object, Vector<String> textureNames, Vector<AnimatedTexture> animatedTextures, Vector<Shader> shaders) {
		super(object, textureNames, animatedTextures, shaders);
	}
	
}
