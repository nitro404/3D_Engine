Note: Make sure that textureDirectories (a text file)
contains a reference to the YOUR GAME'S TEXTURE
DIRECTORY. Otherwise, the converter will prompt
each time it encounters a texture it cannot find...

Similarly, make sure fgdFilePath (also a text file) 
contains a reference to YOUR GAME'S "student.fgd" 
file...

The old versions were directly referencing the
required files assuming you placed all your
stuff in "C:\3d'... The new versions use
relative paths instead...