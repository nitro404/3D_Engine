import java.util.Vector;
import java.io.*;

public class AnimatedTexture {
	
	public int index;
	public String name;
	public int firstTextureIndex;
	public int frames;
	public double speed;
	
	public AnimatedTexture() { }
	
	public AnimatedTexture(int index, String name, String firstTextureName, int frames, int speed, Vector<String> textureFileNames) {
		this.index = index;
		this.name = name;
		this.frames = frames;
		this.speed = speed;
		for(int i=0;i<textureFileNames.size();i++) {
			if(textureFileNames.elementAt(i).equalsIgnoreCase(firstTextureName)) {
				this.firstTextureIndex = i;
				break;
			}
		}
	}
	
	public void writeTo(PrintWriter out) throws Exception {
		out.println("\tAnimatedTexture: " + index + ";");
		out.println("\t\tProperties: 4;");
		out.println("\t\t\t\"name\" => \"" + name + "\"");
		out.println("\t\t\t\"firsttexture\" => \"" + firstTextureIndex + "\"");
		out.println("\t\t\t\"frames\" => \"" + frames + "\"");
		out.println("\t\t\t\"speed\" => \"" + speed + "\"");
	}
	
}
